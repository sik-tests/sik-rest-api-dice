#!/bin/bash

# Source Utilities
. "./hooks/utilities.sh"

# get parameters
operation=$1

case $operation in
"test")
  (./node_modules/.bin/_mocha)
  result=$?
  print_unit_test_broken $result
  ;;
"linter")
  (./node_modules/.bin/eslint App features test *.js)
  result=$?
  print_code_quality_broken $result
  ;;
"coverage")
  (./node_modules/.bin/istanbul cover ./node_modules/mocha/bin/_mocha | grep Statements | grep 100)
  result=$?
  print_coverage $result
  ;; 
"TF")
  (npm start)
  mkdir -p ./coverage/report
  (./node_modules/.bin/cucumber-js features -f json:./coverage/report/cucumber_report.json)
  result=$?
  print_tf $result
  (node ./features/report/generate.js)
  ;;
*)
  echo "Bad parameters for check.sh : ${result}"
  ;;
esac
