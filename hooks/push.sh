#!/bin/bash
#
# For the hook to work, please ln -s this file in
#!/bin/bash
#
# Function to run for the push operation
#

# Source Utilities
. "./hooks/utilities.sh"

# Coverage
(./hooks/check.sh coverage)
coverage=$?

# Linter
(./hooks/check.sh linter)
code_quality_broken=$?

let "result=$coverage||$code_quality_broken"
print_result $result

exit $result
