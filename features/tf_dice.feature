Feature: throw a dice through a REST api

  Scenario Outline: Throw a dice
    Given I open url '/dice/<faces>' then I get a result between 1 and <faces>
    Examples:
    | faces |
    |     4 |
    |     6 |
    |     8 |
    |    10 |
    |    12 |
    |    20 |
    |   100 |

  Scenario Outline: Error management
    Given I open url '/dice<url_end>' then I get an HTTP return code <errorCode>
    Examples:
    | url_end | errorCode |
    |       / |       404 |
    | /chaine |       404 |
    |     /-1 |       400 |
    |      /1 |       400 |
    |    /1.5 |       404 |
