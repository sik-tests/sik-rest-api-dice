/**
 * Module devDependencies
 */
const { Given /* , When, Then */ } = require('cucumber')
const { expect } = require('chai')
const curl = require('curl')
const config = require('../../config')


/**
 * Given step definitions
 */
Given('I open url {string} then I get a result between 1 and {int}', (url, faces, callback) => {
  const fullUrl = config.base_url + url
  curl.get(fullUrl, undefined, (err, resp, body) => {
    const result = JSON.parse(body).de
    expect(result).to.be.a('number')
    expect(result).to.be.at.least(1)
    expect(result).to.be.at.most(faces)
    expect(result % 1).to.be.equal(0)
    callback()
  })
})

Given('I open url {string} then I get an HTTP return code {int}', (url, errCode, callback) => {
  const fullUrl = config.base_url + url
  curl.get(fullUrl, undefined, (err, resp) => {
    const result = resp.statusCode
    expect(result).to.be.equal(errCode)
    callback()
  })
})

/**
  * Then step definitions
  */
