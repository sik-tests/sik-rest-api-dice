class Dice {
  constructor(faces) {
    // Check params. Faces should be an integer > 2
    if (faces === undefined) throw new SyntaxError('Faces is not defined')
    if (typeof (faces) !== 'number') throw new SyntaxError('Faces is not a number')
    if ((faces % 1) !== 0) throw new SyntaxError('Faces is a float')
    if (faces < 2) throw new RangeError('Number of faces is less than 2')
    this.faces = faces
    this.throwDice()
  }

  throwDice() {
    this.result = Math.floor(Math.random() * this.faces) + 1
    return this.result
  }
}

module.exports = Dice
