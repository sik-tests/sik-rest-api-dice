/**
 * Module dependencies
 */
const { expect } = require('chai')
const Dice = require('../App/dice')

const DICE_FACES = 6
const NB_TRIES = 1000

/**
 * tests
 */
describe('Dice', () => {
  it('should take a positive integer > 1 as faces param', () => {
    expect(() => new Dice()).to.throw()
    expect(() => new Dice('string')).to.throw()
    expect(() => new Dice(0)).to.throw()
    expect(() => new Dice(-1)).to.throw()
    expect(() => new Dice(1)).to.throw()
    expect(() => new Dice(6.5)).to.throw()
    // expect(() => new Dice(6)).to.throw()
  })
  it('should always return an integer between 1 and the number of faces', () => {
    const myDice = new Dice(DICE_FACES)
    let result = 0
    for (let i = 0; i <= NB_TRIES; i += 1) {
      result = myDice.throwDice()
      expect(result).to.be.a('number')
      expect(result).to.be.at.least(1)
      expect(result).to.be.at.most(DICE_FACES)
      expect(result % 1).to.be.equal(0)
    }
  })
})
