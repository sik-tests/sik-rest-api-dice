/**
 * Module dependencies
 */
const config = require('./config')
const restify = require('restify')
const errors = require('restify-errors')
const Dice = require('./App/dice')

/**
 * Initialize server
 */
const server = restify.createServer({
  name: config.name,
  version: config.version,
})

/**
 * Treatment
 */
function respond(req, res, next) {
  try {
    const d = new Dice(parseFloat(req.params.faces, 10))
    res.send(200, { de: d.result })
    return next()
  } catch (e) {
    if (e instanceof SyntaxError) {
      return next(new errors.NotFoundError())
    }
    if (e instanceof RangeError) {
      return next(new errors.BadRequestError())
    }
    return next(e)
  }
}

server.get('/dice/:faces', respond)

server.listen(config.port, () => {
  /* eslint-disable no-console */
  console.log('%s listening at %s', server.name, server.url)
  /* eslint-enable no-console */
})
